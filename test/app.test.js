const Cube = require('../src/app').Cube;
const Triangle = require('../src/app').Triangle;
const expect = require('chai').expect;


describe('Testing the Cube Functions', function () {
    it('1. The side length of the Cube', function (done) {
        let c1 = new Cube(2);
        expect(c1.getSideLength()).to.equal(2);
        done();
    });

    it('2. The surface area of the Cube', function (done) {
        let c2 = new Cube(5);
        expect(c2.getSurfaceArea()).to.equal(150);
        done();
    });

    it('3. The volume of the Cube', function (done) {
        let c3 = new Cube(7);
        expect(c3.getVolume()).to.equal(343);
        done();
    });
});
describe('Testing the triangle Functions', function () {
    it('1. The height of the triangle is equal to 4', function (done) {
        let s = new Triangle(3, 4, 5, 0);
        expect(s.getheight()).to.equal(4);
        done();
    });
    it('2. The area of the  triangle is equal to 5', function (done) {
        let a = new Triangle(5, 5, 2, 0);
        expect(a.getSurfaceArea()).to.equal(5);
        done();
    });
    it('2.1 the volume of the prism is equal to 30', function (done){
        let a = new Triangle(5,5,2,6);
        expect(a.getVolume()).to.equal(30);
        done();
    });
    it('3. Wrong variable', function (done) {
        let b = new Triangle(0, 3, 3, 0);
        expect(b.getTypeTriangle()).to.equal('Une des variables est égale à 0, veuillez rentrer une variable supérieure à 0');
        done();
    });
    it('3.1 the triangle is isosceles', function (done) {
        let b = new Triangle(2, 3, 3, 0);
        expect(b.getTypeTriangle()).to.equal('this is an isosceles Triangle');
        done();
    });

    
});
describe('Testing the circle functions', function () {
    it('1. the perimeter of the circle is equal to 26', function (done) {
        let a = new Circle(4);
        expect(a.getPerimeter()).to.equal(26);
        done();
    });
    it('2. the area of the circle is equal to 154', function (done){
        let a = new Circle(7);
        expect(a.getArea()).to.equal (154);
        done();
    });
    it('2. the volume of the sphere is equal to ', function (done){
        let a = new Circle(3);
        expect(a.getSphereVolume()).to.equal (114);
        done();
    });
    

});
