class Cube {
    constructor(length) {
        this.length = length;
    }
    
    getSideLength () {
        return this.length;
    }
    
    getSurfaceArea () {
        return (this.length * this.length) * 6;
    }
    
    getVolume () {
        return Math.pow(this.length,3);
    }
}

class Triangle{
    constructor(base, length1, length2, depth){
        this.base = base;
        this.length1 = length1;
        this.length2 = length2;
        this.depth = depth
    }
    getSideLength () {
        return [this.length1, this.length2, this.base]
        
    } 
    getheight () {
         let s = ((this.base+this.length1+this.length2)/2)
         let height = (Math.sqrt(s*(s - this.base)*(s - this.length1)*(s - this.length2))) / ((1/2)*this.base);
         return height;
    }
    getSurfaceArea () {
        return (Math.round((this.base*this.getheight())/2))
    }
    getVolume () {
        return (this.getSurfaceArea()*this.depth)
    }

    getTypeTriangle (){
        if( this.base == 0 || this.length1 == 0 || this.length2 == 0){
            return('Une des variables est égale à 0, veuillez rentrer une variable supérieure à 0');
        }else if (this.base == this.length1 && this.length1 == this.length2){
            return ('this is a equilateral Triangle');
        } else if (this.length1 == this.length2 && this.length1 != this.base || this.length2 == this.base && this.length2 != this.base || this.length1 == this.base && this.length2 != this.base ){
            return ('this is an isosceles Triangle');
        } else {
            return ('this is an random triangle');
        }
    }

}



module.exports = {
    Cube:Cube,
    Triangle:Triangle
}